from bd import obtenir_connexion
from flask import abort
import journalisation

logger = journalisation.get_log()


class UtilisateurModel:
    def __init__(self, identifiant, mot_de_passe, langue='fr_CA'):
        self.identifiant = identifiant
        self.mot_de_passe = mot_de_passe
        self.langue = langue

    def valider_creation(self):
        try:
            messages = []
            if self.champs_sont_vides():
                messages.append("L'identifiant et le mot de passe ne doivent pas être vides")
                return messages
            if self.utilisateur_existe():
                messages.append("Un utilisateur existe déjà avec cet identifiant")
            return messages
        except Exception as e:
            logger.error(str(e))
            abort(500)

    def valider_authentification(self):
        try:
            messages = []
            if self.champs_sont_vides():
                messages.append("L'identifiant et le mot de passe ne doivent pas être vides")
                return messages
            if not self.utilisateur_existe():
                messages.append("Combinaison identifiant/mot de passe invalide")
            return messages
        except Exception as e:
            logger.error(str(e))
            abort(500)

    def champs_sont_vides(self):
        if not self.identifiant or not self.mot_de_passe:
            return "L'identifiant et le mot de passe ne doivent pas être vides"

    def utilisateur_existe(self):
        connexion = obtenir_connexion()
        try:
            curseur = connexion.cursor()
            curseur.execute('SELECT * FROM utilisateurs WHERE identifiant = %s AND mot_de_passe = %s',
                            (self.identifiant, self.mot_de_passe,))
            if curseur.fetchone() is None:
                return False
            return True
        finally:
            connexion.close()

    def enregistrer(self):
        connexion = obtenir_connexion()
        try:
            curseur = connexion.cursor()
            curseur.execute(
                'INSERT INTO utilisateurs (identifiant, mot_de_passe, est_admin, langue) VALUES (%s, %s, 0, %s)',
                (self.identifiant, self.mot_de_passe, self.langue))
            connexion.commit()
        except Exception as e:
            logger.error(str(e))
            abort(500)
        finally:
            connexion.close()
