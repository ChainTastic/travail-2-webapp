import hashlib
import journalisation
from bd import obtenir_connexion
from models.utilisateur_model import UtilisateurModel
from datetime import date
from babel import (
    numbers,
    dates
)
from flask import (
    Flask,
    render_template,
    url_for,
    request,
    redirect,
    session,
    make_response,
    flash,
    abort
)
from flask_babel import Babel
from models.items_model import ItemModel, db
logger = journalisation.get_log()


def creer_application():
    app_flask = Flask(__name__)
    app_flask.secret_key = "gb[>WZy.pR9*I*F"
    app_flask.config['BABEL_DEFAULT_LOCALE'] = ['fr']
    app_flask.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost:3308/appweb_tp2'
    app_flask.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app_flask)
    with app_flask.app_context():
        db.create_all()
    return app_flask


app = creer_application()
babel = Babel(app)


@babel.localeselector
def get_locale():
    try:
        language = session['langue']
    except KeyError:
        language = None
    if language is not None:
        return language
    return request.accept_languages.best_match(['fr_CA', 'en_CA'])


@app.context_processor
def injecter_date():
    args = request.args
    est_de_retour = request.cookies.get('estDeRetour')
    if "utilisateur" in session:
        format_date = dates.format_date(date.today(), locale=get_locale())
    elif 'region' in args:
        langue = request.args.get('region')
        if 'fr' and 'ca' in langue.lower():
            region = 'fr_CA'
        elif 'en' and '' in langue.lower():
            region = 'en_CA'
        else:
            region = get_locale
        format_date = dates.format_date(date.today(), locale=region)
    elif est_de_retour is None:
        format_date = dates.format_date(date.today(), locale='fr_CA')
    else:
        region = request.cookies.get('region')
        format_date = dates.format_date(date.today(), locale=region)

    return dict(date=format_date)


@app.route('/')
def index():
    try:
        args = request.args
        est_de_retour = request.cookies.get('estDeRetour')
        if "utilisateur" in session:
            identifiant = session['utilisateur']
            resp = make_response(redirect(url_for('accueil', region=session['langue'], estDeRetour=est_de_retour,
                                                  id=identifiant)))
            return resp
        if 'region' in args:
            langue = request.args.get('region')
            if 'fr' and 'ca' in langue.lower():
                region = 'fr_CA'
            elif 'en' and 'ca' in langue.lower():
                region = 'en_CA'
            else:
                region = get_locale
            resp = make_response(redirect(url_for('accueil', region=region, estDeRetour=est_de_retour)))
            return resp
        if est_de_retour is None:
            region = get_locale()
            resp = make_response(redirect(url_for('accueil', region=region)))
            resp.set_cookie('region', region)
            resp.set_cookie('estDeRetour', 'True')
            return resp
        region = request.cookies.get('region')
        resp = make_response(redirect(url_for('accueil', region=region, estDeRetour=est_de_retour)))
        return resp
    except Exception as e:
        logger.error(str(e))
        abort(500)


@app.route('/accueil')
def accueil():
    est_de_retour = request.args.get('estDeRetour')
    identifiant = request.args.get('id')
    injecter_date()
    return render_template('accueil.html', estDeRetour=est_de_retour, identifiant=identifiant)


@app.route('/utilisateurs/creer-compte')
def creation_compte():
    try:
        return render_template('utilisateurs/creer.html')
    except Exception as e:
        logger.error(str(e))
        abort(500)


@app.route('/utilisateurs/creer-compte', methods=['POST'])
def creation_compte_post():
    try:
        identifiant = request.form.get('id')
        mot_de_passe = request.form.get('mot-passe')
        mdp_hache = hashlib.sha256(mot_de_passe.encode()).hexdigest()
        region = request.form.get('region')
        model = UtilisateurModel(identifiant, mdp_hache, region)
        messages = model.valider_creation()
        if len(messages) > 0:
            return render_template('utilisateurs/creer.html', messages=messages)
        model.enregistrer()
        session['utilisateur'] = model.identifiant
        session['langue'] = region
        est_de_retour = request.cookies.get('estDeRetour')
        return redirect(url_for('accueil', estDeRetour=est_de_retour,
                                id=identifiant))
    except Exception as e:
        logger.error(str(e))
        abort(500)


@app.route("/authentification/connexion")
def authentification():
    try:
        return render_template("authentification/authentification.html")
    except Exception as e:
        logger.error(str(e))
        abort(500)


@app.route("/authentification/connexion", methods=["POST"])
def authentification_post():
    try:
        identifiant = request.form.get('id')
        mot_de_passe = request.form.get('mot-passe')
        mot_de_passe_hache = hashlib.sha256(mot_de_passe.encode()).hexdigest()
        model = UtilisateurModel(identifiant, mot_de_passe_hache)
        messages = model.valider_authentification()
        if messages:
            for message in messages:
                flash(message)
            return render_template("authentification/authentification.html")
        session["utilisateur"] = model.identifiant
        connexion = obtenir_connexion()
        curseur = connexion.cursor()
        curseur.execute("SELECT langue FROM utilisateurs WHERE identifiant = %s", (identifiant,))
        langue, = curseur.fetchone()
        curseur.execute("SELECT id FROM utilisateurs WHERE identifiant = %s", (identifiant,))
        uid, = curseur.fetchone()
        session['id'] = uid
        session['langue'] = langue
        connexion.close()
        est_de_retour = request.cookies.get('estDeRetour')
        return redirect(url_for('accueil', estDeRetour=est_de_retour,
                                id=identifiant))
    except Exception as e:
        logger.error(str(e))
        abort(500)


@app.route("/authentification/deconnexion")
def deconnexion():
    try:
        flash('Vous êtes déconnecté')
        session.pop('utilisateur', None)
        session.pop('langue', None)
        session.pop('id', None)
        return redirect(url_for('authentification'))
    except Exception as e:
        logger.error(str(e))
        abort(500)


def internationaliser_nombres(items):
    liste_formatte = []
    for item in items:
        model = ItemModel(item.description, item.quantite)
        quantite_formatte = numbers.format_decimal(model.quantite, locale=get_locale())
        model.quantite = quantite_formatte
        liste_formatte.append(model)
    return liste_formatte


@app.route('/liste')
def liste():
    if 'id' in session:
        liste_items = ItemModel.query.filter_by(utilisateur_id=session['id'])
        items = internationaliser_nombres(liste_items)
        return render_template("items/liste.html", items=items, utilisateur=session['utilisateur'])
    items = internationaliser_nombres(ItemModel.query.all())
    return render_template("items/liste.html", items=items)


@app.route('/items/ajouter')
def creer_item():
    try:
        return render_template("items/creer.html")
    except Exception as e:
        logger.error(str(e))
        abort(500)


@app.route('/items/ajouter', methods=['POST'])
def creer_item_post():
    try:
        nom_item = request.form.get('description')
        quantite = request.form.get('quantite')
        item = ItemModel(description=nom_item, quantite=quantite)
        if 'utilisateur' in session:
            item.utilisateur_id = session['id']
        db.session.add(item)
        db.session.commit()
        return redirect(url_for('liste'))
    except Exception as e:
        logger.error(str(e))
        abort(500)


@app.route('/items/<int:item_id>/effacer')
def effacer_item(item_id):
    try:
        item = ItemModel.query.filter_by(id=item_id).first()
        db.session.delete(item)
        db.session.commit()
        return redirect(url_for('liste'))
    except Exception as e:
        logger.error(str(e))
        abort(500)


if __name__ == '__main__':
    app.run(debug=True)
