import logging


def get_log():
    logger = logging.getLogger('logs')
    logger.setLevel(logging.WARNING)
    fileHandler = logging.FileHandler('app.log')
    fileHandler.setLevel(logging.ERROR)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s -- %(name)s -- %(levelname)s -- %(message)s')
    fileHandler.setFormatter(formatter)
    consoleHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)
    logger.addHandler(consoleHandler)
    return logger
