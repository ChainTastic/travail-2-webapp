from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()


class ItemModel(db.Model):
    __tablename__ = "Item"
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(80), nullable=False)
    quantite = db.Column(db.Float, nullable=False)
    #    utilisateur_id = db.Column(db.Integer, db.ForeignKey('utilisateurs.id'))
    utilisateur_id = db.Column(db.Integer)

    def __repr__(self):
        return '<Item: %r>' % self.description

    def __init__(self, description, quantite):
        self.description = description
        self.quantite = quantite

    def valider(self):
        messages = []
        if not self.description:
            messages.append("La description ne doit pas être vide")
            return messages
        return messages
